#include "bibliotheque.hpp"
#include <iostream>
#include <string>
#include <fstream>
#include <sstream>
#include <algorithm>    // std::sort




const void Bibliotheque::afficher()
	{
		for(Livre livre : *this)
			std::cout<<livre. getTitre()<<livre . getAuteur()<<livre . getAnnee()<<std::endl;
	}
void Bibliotheque::trierParAuteurEtTitre()
	{
		std::sort(begin(), end(),less<Livre>());
		
	
	}
void Bibliotheque::trierParAnnee(){
	
		std::sort(begin(), end(), [](const Livre & a, const Livre & b) -> bool { 
			
			return a.getAnnee() < b.getAnnee(); 
			
			 } );
	}	

const void Bibliotheque::ecrireFichier(const std::string & nomFichier)
	{
		std::ofstream monFlux(nomFichier.c_str());

			if(monFlux)  //On teste si tout est OK
			{
				for(Livre livre : *this)
				   monFlux << livre. getTitre()<< ","<<livre . getAuteur()<< ","<<livre . getAnnee()";"<< endl;
    

			}
			else
			{
				cout << "ERREUR: Impossible d'ouvrir le fichier." << endl;
			}
					
		
		
	}
void lireFichier(const std::string & nomFichier)
	{	
		ifstream monFlux(nomFichier);  //Ouverture d'un fichier en lecture

		if(monFlux)
		{	
			Livre livre;
					
					monFlux >> livre.getTitre();    //Lit un mot depuis le fichier
					
					monFlux >> livre.getAuteur();    //Lit un mot depuis le fichier
					 
					 string age;
					 monFlux >> age;
					 stringstream geek(age); 
					
					int x = 0; 
					
					geek >> x; 
					
					livre.getAnnee()=x;  //Lit un int  depuis le fichier
					
		}
		else
		{
				cout << "ERREUR: Impossible d'ouvrir le fichier en lecture." << endl;
		}
		
		
		
		
	}
	
