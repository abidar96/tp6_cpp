#ifndef LIVRE_
#define LIVRE_

#include <vector>
#include <string>
#include <iostream>

using namespace std;

class Livre {
	private:
		string  _titre;
		string _auteur;
		int _annee;
		//vector<Livre> _figures;
	public:
	Livre();
	Livre(const string& titre,const string& auteur,int annee);
	const string& getTitre()const;
	const string& getAuteur()const;
	const int getAnnee() const;
	bool operator <(const Livre& L2) const; 
	bool operator ==(const Livre& L2)const; 
	
	
	
	
	};
	std::istream& operator >>(std::istream& is, Livre& livre);
	std::ostream& operator <<(  ostream& os, Livre& livre );

#endif
