#ifndef BIBLIOTHEQUE_
#define BIBLIOTHEQUE_


#include <vector>
#include <string>
#include <iostream>
#include "Livre.hpp"

using namespace std;

class Bibliotheque : public std::vector<Livre> {
	//private:
		//vector<Livre> livres;
		
	public:
		const void afficher();
		void trierParAuteurEtTitre();
		void trierParAnnee();
		const void ecrireFichier(const std::string & nomFichier);
		void lireFichier(const std::string & nomFichier);
	
	};
	

#endif
