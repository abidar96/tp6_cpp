#include "Livre.hpp"
#include <iostream>
#include <string>



using namespace std;

Livre::Livre(const string& titre,const string& auteur,int annee){
	
		if(titre.find(";") != std::string::npos )
			throw string("erreur : titre non valide (';' non autorisé)");
		else if(titre.find("\n") != std::string::npos )
			throw string("erreur : titre non valide ('\n' non autorisé)");
		else if(auteur.find("\n") != std::string::npos  )
			throw string("erreur : auteur non valide ('\n' non autorisé)");
		else if( auteur.find(";") != std::string::npos)
			throw string("erreur : auteur non valide (';' non autorisé)");
		
		
		
		else{
			_titre=titre;
			_auteur=auteur;
			_annee=annee;
			}
        
   
}
const string& Livre::getTitre() const {
	return _titre;
	}
const string& Livre::getAuteur() const {
	return _auteur;
	}
const int Livre::getAnnee() const{
	return _annee;
	}
bool Livre::operator < (const Livre & L2) const{
	if(getAuteur() < L2.getAuteur())
		return true;
	else if(getAuteur()==(L2.getAuteur()))
		if(getTitre() < L2.getTitre())
			return true;
	else return false;
	}
bool Livre::operator == (const Livre & L2) const {
	if(getAuteur()== L2.getAuteur() && getAnnee() == L2.getAnnee() && getTitre() == L2.getTitre())	
		return true;
	else return false;
	
	}
/*std::istream&  operator >>(std::istream& is, Livre& livre){
		std::getline(is,livre.getTitre(),";");
		std::getline(is,livre.getAuteur(),";");
		string tmp;
		std::getline(is,tmp& ,";");
		int i = std::stoi( tmp );
		
	return is ;
}*/

std::ostream& operator <<( ostream& os, Livre& livre ) { 
         os << livre.getTitre() << ";" << livre.getAuteur() << ";" << livre.getAnnee();
         return os;            
      }







